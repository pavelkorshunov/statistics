import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';
import Grid from '@material-ui/core/Grid';
import AnswerChoice from '../answerchoice/AnswerChoice'
import './Answer.css'

const styles = theme => ({
	root: {
		flexGrow: 1
	}
});

class Answer extends Component {
	state = {
		checked: [0],
	};

	data = ["Google Chorome", "Firefox", "Opera", "Edge"];

	handleToggle = value => () => {
		const { checked } = this.state;
		const currentIndex = checked.indexOf(value);
		const newChecked = [...checked];

		if (currentIndex === -1) {
			newChecked.push(value);
		} else {
			newChecked.splice(currentIndex, 1);
		}

		this.setState({
			checked: newChecked,
		});
	};

	render() {
		const { classes } = this.props;

		return (
			<div className="Answer">
				<div className={classes.root}>
					<Grid container spacing={24}>
						<Grid item xs={12} sm={6}>
							<div className="Answer-List Answer-List_left">
								<span className="Answer-List__title">Choice</span>
								<hr/>
								<List>
									{this.data.map(value => (
									<ListItem key={value} role={undefined} dense button onClick={this.handleToggle(value)}>
										<Checkbox
										checked={this.state.checked.indexOf(value) !== -1}
										tabIndex={-1}
										disableRipple
										/>
										<ListItemText primary={`${value}`} />
									</ListItem>
									))}
								</List>
							</div>
						</Grid>
						<Grid item xs={12} sm={6}>
							<div className="Answer-List Answer-List_right">
								<span className="Answer-List__title">Your answer</span>
								<hr/>
								<AnswerChoice list={this.state} />
							</div>
						</Grid>
					</Grid>
				</div>
			</div>
		);
	}
}

Answer.propTypes = {
	classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Answer);