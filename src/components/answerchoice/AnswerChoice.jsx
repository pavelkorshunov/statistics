import React, {Component} from "react";
import './AnswerChoice.css';

class AnswerChoice extends Component {
    render() {
        const { checked } = this.props.list;
        return (
            <ul className="AnswerChoice">
                {checked.map(value => (
                    value ? <li key={value} className="AnswerChoice__list">
                        <span className="AnswerChoice__text">{value}</span>
                    </li> : ""
                ))}
            </ul>
        );
    }
}

export default AnswerChoice;