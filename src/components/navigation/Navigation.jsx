import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

const styles = {
	root: {
		flexGrow: 1,
	},
	grow: {
		flexGrow: 1,
	},
};

class Navigation extends Component {
	render() {
		const { classes } = this.props;
		return (
			<div className="Navigation">
				<div className={classes.root}>
					<AppBar position="static" color="primary">
					<Toolbar>
						<Typography variant="h6" color="inherit" className={classes.grow}>
							Statistics
						</Typography>
						<Button color="inherit">Login</Button>
					</Toolbar>
					</AppBar>
				</div>
			</div>
		);
	}
}

Navigation.propTypes = {
	classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Navigation);