import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import './Question.css';

const styles = theme => ({
	root: {
		...theme.mixins.gutters(),
		paddingTop: theme.spacing.unit * 2,
		paddingBottom: theme.spacing.unit * 2,
	},
});

class Question extends Component {
	render() {
		const { classes } = this.props;
		return (
			<div className="Question">
				<Paper className={classes.root} elevation={1}>
					<Typography variant="h5" component="h3">
						Which browser do you use?
					</Typography>
				</Paper>
			</div>
		);
	}
}

Question.propTypes = {
	classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Question);