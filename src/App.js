import React, { Component } from 'react';
import Navigation from './components/navigation/Navigation';
import Question from './components/question/Question';
import Answer from './components/answer/Answer';

class App extends Component {
	render() {
		return (
			<div className="main">
				<Navigation/>
				<div className="wrapper">
					<Question/>
					<Answer/>
				</div>
			</div>
		);
	}
}

export default App;
